import React from 'react';
import PropTypes from 'prop-types';
import {View, Text} from 'react-native';

const Congrats = (props) => {
  return props.success ? (
    <View
      style={{
        backgroundColor: 'lime',
        width: '100%',
        marginTop: 20,
        borderRadius: 5,
        alignSelf: 'center',
      }}
      testID="component-congrats">
      <Text
        style={{fontSize: 20, textAlign: 'center', opacity: 0.8}}
        testID="congrats-message">
        Congratulations! You guessed the word!
      </Text>
    </View>
  ) : (
    <View testID="component-congrats"></View>
  );
};

Congrats.propTypes = {
  success: PropTypes.bool.isRequired,
};

export default Congrats;
