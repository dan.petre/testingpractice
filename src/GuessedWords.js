import React from 'react';
import PropTypes from 'prop-types';
import {View, Text} from 'react-native';

const GuessedWords = (props) => {
  let contents;
  if (props.guessedWords.length === 0) {
    contents = (
      <Text testID="guessed-instructions">Try to guess the secret word!</Text>
    );
  } else {
    const guessedWordRows = props.guessedWords.map((word, index) => (
      <View
        style={{
          marginTop: 10,
          flexDirection: 'row',
          justifyContent: 'space-around',
        }}
        testID="guessed-word"
        key={index}>
        <Text>{word.guessedWord}</Text>
        <Text>{word.letterMatchCount}</Text>
      </View>
    ));
    contents = (
      <View style={{marginTop: 20}} testID="guessed-words">
        <Text style={{fontSize: 20}}>Guessed Words</Text>
        <View
          style={{
            marginTop: 10,
            flexDirection: 'row',
            justifyContent: 'space-around',
            backgroundColor: 'lightgray',
          }}>
          <Text>Guess</Text>
          <Text>Matching Letters</Text>
        </View>
        <View style={{}}>{guessedWordRows}</View>
      </View>
    );
  }
  return <View testID="component-guessed-words">{contents}</View>;
};

GuessedWords.propTypes = {
  guessedWords: PropTypes.arrayOf(
    PropTypes.shape({
      guessedWord: PropTypes.string.isRequired,
      letterMatchCount: PropTypes.number.isRequired,
    }),
  ).isRequired,
};

export default GuessedWords;
