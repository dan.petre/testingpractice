import {actionsTypes} from '../actions';

export default (state = null, action) => {
  switch (action.type) {
    case actionsTypes.SET_SECRET_WORD:
      return action.payload;
    default:
      return state;
  }
};
