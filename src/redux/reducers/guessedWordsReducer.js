import {actionsTypes} from '../actions';

export default (state = [], action) => {
  switch (action.type) {
    case actionsTypes.GUESS_WORD:
      return [...state, action.payload];
    default:
      return state;
  }
};
