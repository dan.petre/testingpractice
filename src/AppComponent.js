import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import Congrats from './Congrats';
import GuessedWords from './GuessedWords';
import Input from './Input';

import {connect} from 'react-redux';
import {getSecretWord} from './redux/actions';

const AppComponent = (props) => {
  useEffect(() => {
    console.log('The PROPS received: ', props);
  });
  return (
    <View
      style={{
        flex: 1,
        marginTop: 100,
        marginHorizontal: 16,
      }}>
      <Text style={{alignSelf: 'center', fontSize: 60}}>Jotto</Text>
      <Congrats success={props.success} />
      <Input />
      <GuessedWords
        guessedWords={[{guessedWord: 'train', letterMatchCount: 3}]}
      />
    </View>
  );
};

const mapStateToProps = (props) => {
  const {success, secretWord, guessedWords} = props;
  return {success, secretWord, guessedWords};
};

export default connect(mapStateToProps, {getSecretWord})(AppComponent);
