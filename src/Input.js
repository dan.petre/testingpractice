import React, {useEffect} from 'react';
import {View, TextInput, Button} from 'react-native';
import {useSelector, connect} from 'react-redux';
import {guessWord} from './redux/actions';

const Input = (props) => {
  useEffect(() => {
    console.log('The Props from Input Component', props);
  });
  // const {success} = useSelector((state) => state.success);
  // const content = props.success ? null : (
  //   <View>
  //     <TextInput
  //       testID="input-box"
  //       style={{height: 40, borderColor: 'gray', borderWidth: 1}}
  //       placeholder="Enter guess"
  //     />
  //     <Button testID="submit-button" title="Submit" />
  //   </View>
  // );

  return (
    <View testID="component-input">
      {props.success ? null : (
        <View>
          <TextInput
            testID="input-box"
            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
            placeholder="Enter guess"
          />
          <Button testID="submit-button" title="Submit" />
        </View>
      )}
    </View>
  );
};

const mapStateToProps = ({success}) => {
  return {success};
};

export default connect(mapStateToProps, {guessWord})(Input);

// export default Input;
