import React, {useState} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const App = () => {
  const [count, setCount] = useState(0);
  const [error, setError] = useState(null);

  const handleDecrement = () => {
    if (count === 0) {
      setError(`The counter can't go below zero.`);
      return;
    }
    setCount(count - 1);
  };

  const handleIncrement = () => {
    if (error) {
      setError(null);
    }
    setCount(count + 1);
  };

  return (
    <View testID="component-app" style={styles.container}>
      <View>
        {error && <Text>{error}</Text>}
        <Text testID="counter-display" style={{fontSize: 30}}>
          The count is {count}
        </Text>

        <Button
          testID="increment-button"
          onPress={handleIncrement}
          title="Increment counter"
        />
        <Button
          testID="decrement-button"
          onPress={handleDecrement}
          title="Decrement counter"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 100,
  },
});

export default App;
