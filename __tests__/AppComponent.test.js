import React from 'react';
import {shallow} from 'enzyme';

import AppComponent from '../src/AppComponent';
import {storeFactory} from '../src/utils/testUtils';

const setup = (state = {}) => {
  const store = storeFactory(state);
  const wrapper = shallow(<AppComponent store={store} />);
  return wrapper;
};

describe('redux properties', () => {
  test('has access to `success` state', () => {
    const success = true;
    const wrapper = setup({success});
    const successProp = wrapper.props().children.props.success;
    expect(successProp).toBe(true);
  });
  test('has access to `secretWord` state', () => {
    const secretWord = 'party';
    const wrapper = setup({secretWord});
    const successProp = wrapper.props().children.props.secretWord;
    expect(successProp).toBe('party');
  });
  test('access to `guessedWords` state', () => {
    const guessedWords = [{guessedWord: 'train', letterMatchCount: 3}];
    const wrapper = setup({guessedWords});
    const guessedWordsProp = wrapper.props().children.props.guessedWords;
    expect(guessedWordsProp).toEqual(guessedWords);
  });
  test('`getSecretWord` action creator is a function on the props', () => {
    const wrapper = setup();
    const getSecretWordProp = wrapper.props().children.props.getSecretWord;
    expect(getSecretWordProp).toBeInstanceOf(Function);
  });
});
