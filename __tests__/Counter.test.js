/**
 * @jest-environment jsdom
 */

import 'react-native';
import React from 'react';
import {Text} from 'react-native';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Counter from '../src/Counter';

Enzyme.configure({adapter: new Adapter()});

/*
 * Factory function to create a ShallowWrapper for the Counter component.
 * @function setup
 *
 */
const setup = () => shallow(<Counter />);

const findByTestAtrr = (wrapper, val) => wrapper.find(`[testID='${val}']`);

it('renders correctly', () => {
  const wrapper = setup();
  const appComponent = findByTestAtrr(wrapper, 'component-app');
  expect(appComponent.length).toBe(1);
});

it('renders button', () => {
  const wrapper = setup();
  const appComponent = findByTestAtrr(wrapper, 'increment-button');
  expect(appComponent.length).toBe(1);
});

it('renders counter display', () => {
  const wrapper = setup();
  const appComponent = findByTestAtrr(wrapper, 'counter-display');
  expect(appComponent.length).toBe(1);
});

it('renders text component', () => {
  const wrapper = shallow(<Text>Hello, world</Text>);
  expect(wrapper.text()).toEqual('Hello, world');
});

it('renders starts at 0', () => {
  const wrapper = setup();
  const count = findByTestAtrr(wrapper, 'counter-display').dive().text();
  expect(count).toBe('The count is 0');
});

it('should increment counter display when clicking the increment button', () => {
  const wrapper = setup();
  // find the button
  const button = findByTestAtrr(wrapper, 'increment-button').dive();
  // click the button
  button.simulate('press');

  // find the display, and test that the number has been incremented
  const count = findByTestAtrr(wrapper, 'counter-display').dive().text();
  expect(count).toBe('The count is 1');
});
