import {actionTypes} from '../../../src/redux/actions';
import successReducer from '../../../src/redux/reducers/successReducer';

test('returns default initial state of `false` when no action is passed', () => {
  const newState = successReducer(undefined, {});
  expect(newState).toBe(false);
});

test('returns state of true upon receiving an action type `CORRECT_GUESS`', () => {
  const newState = successReducer(undefined, {type: 'CORRECT_GUESS'});
  expect(newState).toBe(true);
});
